"use strict";
exports.CONTATOS = [
    { id: 1, nome: 'Fulano de Tal', email: 'fulano@email.com', telefone: '(00) 0000-0000' },
    { id: 2, nome: 'Ciclano', email: 'ciclano.com', telefone: '(00) 0000-0000' },
    { id: 3, nome: 'Seu Madruga', email: 'madrugiunha@email.com', telefone: '(00) 0000-0000' },
    { id: 4, nome: 'Papito', email: 'papito@email.com', telefone: '(00) 0000-0000' },
    { id: 5, nome: 'Bob Esponja ', email: 'esponjabob@email.com', telefone: '(00) 0000-0000' },
];
//# sourceMappingURL=contatos-mock.js.map