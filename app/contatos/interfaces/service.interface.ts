export interface ServiceInterface<T> {
    findAll(): Promise<T[]>;
    find(id:number): Promise<T>;
    create(Object: T): Promise<T>;
    update(Object: T): Promise<T>;
    delete(Object: T): Promise<T>;
}