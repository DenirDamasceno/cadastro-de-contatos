import {Component, EventEmitter, OnInit, Input, OnChanges,  SimpleChanges, Output  } from '@angular/core';
import { Contato } from './contato.model';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { ContatoService } from './contato.service';
import 'rxjs/add/observable/of';
import {Router} from '@angular/router';
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';


@Component({
    moduleId: module.id,
    selector: 'contato-busca',
    templateUrl: 'contato-busca.component.html',
    styles: [`
        .cursror-pointer: hover {
            cursor: pointer;
        }
    `]

})

export class ContatoBuscaComponent implements OnInit, OnChanges{

    @Input()busca : string; 
    @Output()buscaChange: EventEmitter<string> = new EventEmitter<string>();
    contatos : Observable<Contato[]>;
    private termosDaBusca: Subject<string> = new Subject<string>();

    constructor (
        private contatoService: ContatoService,
        private router : Router 
    ){ }

    ngOnInit(): void {
        this.contatos = this.termosDaBusca
        .debounceTime(500) // Aguarde por 500ms para emitir novos eventos
        .distinctUntilChanged() // Ignore se o proximo termo for igual ao anterior
        .switchMap(term =>  term ? this.contatoService.search(term) : Observable.of<Contato[]>([]))
        .catch(err => {
            return Observable.of<Contato[]>([]);
        });

        
    }

    ngOnChanges( changes: SimpleChanges): void{
        let busca: SimpleChange = changes ['busca'];
        this.search (busca.currentValue);
    }

    search(termo: string) : void{
        this.termosDaBusca.next(termo);
        this.buscaChange.emit(termo);
    }

    verDetalhe(contato: Contato): void {
        let link  = ['contato/save', contato.id];
        this.router.navigate(link);
        this.buscaChange.emit('');  
    }

}