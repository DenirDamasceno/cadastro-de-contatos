import './ultil/rxjs-extensions';

import { NgModule } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";
import {HttpModule} from '@angular/http';
import {AppComponent} from './app.component'
import {ContatosModule} from './contatos/contatos.module';
import {AppRoutingModule} from './app-routing.module';
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './contatos/in-memory-data-service';
import {DialogService} from './contatos/dialog.service';


@NgModule({
    imports: [
        BrowserModule,
        ContatosModule,
        FormsModule,
        AppRoutingModule,
        HttpModule,
        InMemoryWebApiModule.forRoot(InMemoryDataService)
        
    ],
    declarations: [AppComponent],
    providers:[
        DialogService
    ],
    bootstrap: [AppComponent],
})

export class AppModule {}